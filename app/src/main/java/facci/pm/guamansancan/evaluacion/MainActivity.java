package facci.pm.guamansancan.evaluacion;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import facci.pm.guamansancan.evaluacion.Adapter.AdapterEstudiante;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private static final String URL_ESTUDIANTE = "http://10.1.15.127:3003/estudiantes";
    private RecyclerView.LayoutManager layoutManager;
    private AdapterEstudiante adapterEstudiante;
    private ArrayList<Estudiante> arrayListE;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView)findViewById(R.id.estudiante);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        arrayListE = new ArrayList<>();
        adapterEstudiante = new AdapterEstudiante(arrayListE);
        progressDialog = new ProgressDialog(this);

        obtenerEstudiante();
    }


    public void obtenerEstudiante(){
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_ESTUDIANTE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response);
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObject =jsonArray.getJSONObject(i);
                        Estudiante estudiante = new Estudiante();
                        estudiante.setImage(jsonObject.getString("imagen"));
                        estudiante.setId(jsonObject.getString("id"));
                        estudiante.setNombres(jsonObject.getString("nombres"));
                        estudiante.setApellidos(jsonObject.getString("apellidos"));
                        estudiante.setParcial_uno(jsonObject.getString("parcialuno"));
                        estudiante.setParcial_dos(jsonObject.getString("parcialdos"));
                        estudiante.setAprueba(jsonObject.getString("aprueba"));
                        arrayListE.add(estudiante);
                    }
                    recyclerView.setAdapter(adapterEstudiante);
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Ocurrio un error mientras se cargaba", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
