package facci.pm.guamansancan.evaluacion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Loguin extends AppCompatActivity {

    EditText edtp;
    Button btnb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loguin);

        edtp = (EditText) findViewById(R.id.edtp);
        btnb = (Button) findViewById(R.id.btnb);
    }

    public void boton (View view){
        Intent intent = new Intent(Loguin.this, MainActivity.class);
        startActivity(intent);
    }
}
