package facci.pm.guamansancan.evaluacion;


public class Estudiante {


    private String id;
    private  String nombres;
    private String apellidos;
    private String parcial_uno;
    private String parcial_dos;
    private String aprueba;
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getParcial_uno() {
        return parcial_uno;
    }

    public void setParcial_uno(String parcial_uno) {
        this.parcial_uno = parcial_uno;
    }

    public String getParcial_dos() {
        return parcial_dos;
    }

    public void setParcial_dos(String parcial_dos) {
        this.parcial_dos = parcial_dos;
    }

    public String getAprueba() {
        return aprueba;
    }

    public void setAprueba(String aprueba) {
        this.aprueba = aprueba;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
