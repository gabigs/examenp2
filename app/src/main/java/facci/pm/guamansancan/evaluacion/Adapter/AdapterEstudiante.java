package facci.pm.guamansancan.evaluacion.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import facci.pm.guamansancan.evaluacion.Estudiante;
import facci.pm.guamansancan.evaluacion.R;

public class AdapterEstudiante extends RecyclerView.Adapter<AdapterEstudiante.MyViewHolder> {

    private ArrayList<Estudiante> estudiante;

    public AdapterEstudiante(ArrayList<Estudiante> estudiante){this.estudiante = estudiante;}


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        Estudiante estudiante1 = estudiante.get(i);
        Picasso.get().load(estudiante1.getImage()).into(myViewHolder.Image);
        myViewHolder.Id.setText(estudiante1.getId());
        myViewHolder.Nombres.setText(estudiante1.getNombres());
        myViewHolder.Apellidos.setText(estudiante1.getApellidos());
        myViewHolder.ParcialUno.setText(estudiante1.getParcial_uno());
        myViewHolder.ParcialDos.setText(estudiante1.getParcial_dos());
        myViewHolder.Aprueba.setText(estudiante1.getAprueba());

    }

    @Override
    public int getItemCount() {return estudiante.size();}

    public class MyViewHolder extends RecyclerView.ViewHolder{
         private ImageView Image;
         private TextView Id, Nombres, Apellidos, ParcialUno, ParcialDos, Aprueba;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            Image = (ImageView) itemView.findViewById(R.id.Imagen);
            Id = (TextView) itemView.findViewById(R.id.Id);
            Nombres = (TextView) itemView.findViewById(R.id.Nombres);
            Apellidos = (TextView) itemView.findViewById(R.id.Apellidos);
            ParcialUno = (TextView) itemView.findViewById(R.id.ParcialUno);
            ParcialDos = (TextView) itemView.findViewById(R.id.ParcialDos);
            Aprueba = (TextView) itemView.findViewById(R.id.Aprueba);

        }
    }
}
